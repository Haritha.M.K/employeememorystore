class Node:
    def __init__(self, data, nextNode=None):
        self.data = data
        self.nextNode = None


class Employee:
    def __init__(self, empid, firstname, lastname, dept):
        self.empid = empid
        self.firstname = firstname
        self.lastname = lastname
        self.dept = dept


class LinkedList:
    def __init__(self):
        self.headval = None

    def InsertAtEnd(self, empid, firstname, lastname, dept):
        e = Employee(empid, firstname, lastname, dept)
        NewNode = Node(e)
        if self.headval is None:
            self.headval = NewNode
            return NewNode.data.empid
        last = self.headval
        while last.nextNode:
            last = last.nextNode
        last.nextNode = NewNode
        return NewNode.data.empid

    def printEmpData(self):
        currentnode = self.headval
        while currentnode is not None:
            print(currentnode.data.empid)
            print(currentnode.data.firstname)
            print(currentnode.data.lastname)
            print(currentnode.data.dept)
            print()
            currentnode = currentnode.nextNode

    def search(self, Id):

        current = self.headval

        while current != None:
            if current.data.empid == Id:
                print("EmpId ", Id, " is Found and details are as follows")
                return current

            current = current.nextNode

        return False

    def updateemp(self, Id):

        current = self.headval

        while current != None:
            if current.data.empid == Id:
                print("EmpId ", Id, " is Found and give details to update")
                current.data.firstname = input("Enter ur firstname: ")
                current.data.lastname = input("Enter ur lastname: ")
                current.data.dept = input("Enter ur department: ")
                print()
                return True

            current = current.nextNode

        return False

class sortLinkedList:

    def __init__(self):
        self.headval = None
        self.lastval = None

    def printEmpData(self):
        currentnode = self.headval
        while currentnode is not None:
            print(currentnode.data.empid)
            print(currentnode.data.firstname)
            print(currentnode.data.lastname)
            print(currentnode.data.dept)
            print()
            currentnode = currentnode.nextNode

    def AddEmpInOrder(self,empid, firstname, lastname, dept):
        e1 = Employee(empid, firstname, lastname, dept)
        EmpNode = Node(e1)
        if self.headval is None:
            self.headval = EmpNode
            return
        last = self.headval
        while last.nextNode:
            last = last.nextNode
        last.nextNode = EmpNode
        return True

    def sortEmpList(self,empList):
        current = empList.headval
        List = []
        i = 0
        if current is None:
            return
        while current is not None:
            List.append(current.data.firstname)
            current = current.nextNode
        List.sort()
        print(List)
        length = len(List)
        while i < length:
            current = empList.headval
            while current is not None:
                if current.data.firstname == List[i]:
                    self.AddEmpInOrder(current.data.empid, current.data.firstname, current.data.lastname,
                                       current.data.dept)
                    break
                current = current.nextNode
            i += 1

        return True

    def deleteEmpNode(self, key):
        temp = self.headval
        if (temp is not None):
            if (temp.data.empid == key):
                self.headval = temp.nextNode
                temp = None
                return
        while (temp is not None):
            if temp.data.empid == key:
                break
            prev = temp
            temp = temp.nextNode
        if (temp == None):
            return
        prev.nextNode = temp.nextNode
        temp = None

    def getSizeOfLL(self):
        curr = self.headval  # Initialise temp
        count = 0  # Initialise count

        # Loop while end of linked list is not reached
        while curr is not None:
            count += 1
            curr = curr.nextNode
        return count


if __name__ == '__main__':

    print()
    empList = LinkedList()
    sortEmpList = sortLinkedList()
    print("****EMPLOYEE INFORMATION PROCESSING****")
    print()
    TotalNumOfEmp = int(input("Enter Total Number of Employees to be Inserted: "))
    # print("****INSERT EMPLOYEE DETAILS****")
    i = 0
    empid = 0
    while i < TotalNumOfEmp:
        empid += 1
        firstname = input("Enter ur firstname: ")
        lastname = input("Enter ur lastname: ")
        dept = input("Enter ur department: ")
        EmployeeID = empList.InsertAtEnd(empid, firstname, lastname, dept)
        print("Your Empid: ",EmployeeID)
        print()
        i += 1

    print("****EMPLOYEE INFORMATION****")
    empList.printEmpData()

    print("****SEARCHING EMPLOYEE****")
    Id = int(input("Enter Employee ID to search for: "))
    e=empList.search(Id)
    if e:
        print("FirstName: ",e.data.firstname)
        print("LastName: ",e.data.lastname)
        print("Department: ",e.data.dept)
    print()
    print("****UPDATING EMPLOYEE INFO****")
    Id = int(input("Enter Employee ID to update: "))
    empList.updateemp(Id)
    print()
    print("****SORTED EMPLOYEE INFO****")
    sortEmpList.sortEmpList(empList)
    sortEmpList.printEmpData()
    print()
    print("****DELETING EMPLOYEE INFO****")
    Id = int(input("Enter Employee ID to delete: "))
    delteinfo = sortEmpList.deleteEmpNode(Id)
    sortEmpList.printEmpData()
    print()
    LinkedListSize = sortEmpList.getSizeOfLL()
    print("Size of Employee Linked List: ", LinkedListSize)
