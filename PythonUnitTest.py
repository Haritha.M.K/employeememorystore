import unittest
from EmployeeStore import LinkedList,Node,Employee,sortLinkedList
employeeList = LinkedList()
sortList = sortLinkedList()


class EmployeeTestCase(unittest.TestCase):

    def test_InsertEmp(self):
        input = employeeList.InsertAtEnd(1, "anu", "kumar", "it")
        self.assertEqual(input,1)
        input = employeeList.InsertAtEnd(2, "banu", "kumar", "it")
        self.assertEqual(input, 2)
        input = employeeList.InsertAtEnd(3, "rekha", "amar", "it")
        self.assertEqual(input, 3)

    def test_search(self):
        self.assertEqual(employeeList.search(4), False)

    def test_getNumberofEmployees(self):
        self.assertEqual(sortList.getSizeOfLL(), 0)


if __name__ == '__main__':
    unittest.main()
