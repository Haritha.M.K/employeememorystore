# EmployeeMemoryStore

To implement an in-memory store for employees using a custom built singly-linked list. It searches for an employee, print the store content after sorting and do additional functions like updating.